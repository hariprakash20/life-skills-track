# Git commands
## add
```
git add A.txt
```
* It is used transfer the files to the staging area.
  
## commit
```
git commit -m "Commit B.txt file"
```
It is used to save the files in the local repository.

## clone
```
git clone "ssh link".
```
It is used to clone a repository in github/gitlab locally.

## push

```
git push origin main
```
It is used to the files from local repository to the remote repository.

## pull

```
git pull <remote>
```
It is used to fetch the specified remote branch from the remote repository.

## status

```
git status
```
It displays the state of the working directory and the staging area.

## checkout

```
git checkout 
```
It in navigation between the branches in the repository.

## branch

```
git branch
```
It displays all the branches in the repository.

## diff

```
git diff branch-name
```
It displays the difference between the current branch and the sepcifed branch.

## merge
```
git merge branch-name
```
It merges the current branch with the specified branch.

## log
```
git log 
```
It displays histroy of commits with details.

## stash 

```
git stash
```
It takes the uncommitted and unstaged changes, and saves them for later use, and then reverts them from the working copy.