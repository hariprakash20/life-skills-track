# Tiny habits
## Question 1 - Your takeaways from the video (Minimum 5 points).
* If we want to change our life we need to break it out into tiny behaviors and put them in the right place.
* To build a habit we can change our environment and make a very tiny habit and increase it daily.
* Motivation is temporary, we should not rely on it for changing our behavior.
* Celebrating after a small achievement helps us keep up with the habit.
* Our life can be changed by creating tiny habits.
three things to create a habit
    * Motivation
    * Ability
    * Trigger that polls the action.
## Question 2 - Your takeaways from the video in as much detail as possible
* Behavior is a thing that we do at any point in time like we can put our phone away one hour before bed helps us sleep better.
* The small changes that change everything is an aspiration but, it is impossible to achieve at any given moment like you cannot get better sleep suddenly.
* Motivation is your desire, Ability is your capacity, the prompt is your cue
* To sustain and grow our habit we must generate the feeling of shine after we execute our tiny habit.
## Question 3 - How can you use B = MAP to make making new habits easier?
B = behavior
M = Motivation
A = Ability
P = Prompt

To make new habits easier
* Make the goal into tiny habits.
* Celebrate the successful completion of every small task.
  
## Question 4 - Why it is important to "Shine" or Celebrate after each successful completion of a habit?
* To boost our confidence and motivation quickly.
* celebration strengthens the roots of a specific habit.
* celebration teaches us how to be nice to ourselves
## Question 5 - Your takeaways from the video (Minimum 5 points)
* Small improvements can achieve big things.
* Consistency is the key to achievement.
* Set your environment for good behaviors.
* The new habits should be obvious, attractive, easy, and satisfying.
* We should give our goals time and place to grow.
* 1% growth every day will walk a huge difference in the long run.
## Question 6 - Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.
There are three layers of behavior change:
* Identity - This layer is concerned with changing your beliefs like self-image.
* Process - This layer is about changing your habits and routine like going to the gym.
* Outcomes - This layer is about changing your results like losing weight.
## Question 7 - Write about the book's perspective on how to make a good habit easier.
 To make a good habit easier we need to make it obvious, Attractive, EasySatisfying
For doing that we can :
* We should start with small initiatives
* Look for our triggers and obstacles
* Celebrate small achievements
* Work setting our environment
* We should keep a record of our progress.
* We should focus on one habit at a time.
## Question 8 - Write about the book's perspective on making a bad habit more difficult.
  To make a bad habit difficult we need to make it invisible, rough, hard, and unsatisfying for doing that we can:
* We can avoid triggers.
* Reduce our cravings.
* Adopt other healthy routines
* Swap a bad habit for a better habit
## Question 9: - Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
* I want to create a habit of exercising.
* I will fix a time in a day to work out.
* I will track my progress using apps like a habit tracker.
* I will have a small goal to achieve every day and slowly improve it.
## Question 10 - Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
I want to go to sleep at night before 11:00 pm.
* I will stay away from screens that will not allow me to sleep.
* I will be regular so that my sleep cycle is fixed and it will be easier for me to go to sleep early.