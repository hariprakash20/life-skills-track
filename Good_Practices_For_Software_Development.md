# Good Practices For Software Development

## Question 1 - What is your one major takeaway from each one of the 6 sections. So 6 points in total.

* Hearing the requirement and understanding the task to be performed is very crucial because if we get it wrong we will be wasting so much time, money and effort on the wrong front.
* Always let any of your team member/members your current status, progress or your difficulties.
* We must learn to communicate our problems in a clear and understandable way to others.
* Knowing the team members will help us plan upcoming events effectively.
* Communication with the team members should be not exceed for hours because everyone has their own work to do.
* 100% involvement should be given to our work and we should avoid usage of social media or other distracting activities during work time.

## Question 2 - Which area do you think you need to improve on? What are your ideas to make progress in that area?

  Everything above can be addressed or improved so,
  * I'll care a small note and pen with myself at all times so that whenever there is update in the requirement, it will be noted.
  * I'll be updating progress now and then to my team member or my superior.
  * I'll start using screenshots and websites like codepen for doubt calrification.
  * I'll start using Booster app to improve my productivity. 