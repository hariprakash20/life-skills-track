# Prevention of Sexual Harassment
## What kinds of behavior cause sexual harassment?
There are two types of harassment verbal, visual, and physical. 
* Verbal harassment involves inappropriate comments or jokes on a person's body or clothing, asking for sexual favors, threats, spreading rumors, and using foul language.
* Visual harassment involves the usage of posters, drawings, screensavers, cartoons, emails, and texts which is inappropriate.
* Physical harassment involves touching someone without their consent, in an inappropriate way, or discomforting way.

## What would you do in case you face or witness any incident or repeated incidents of such behavior?

* firstly I would warn the person to stop and do not repeat the action. If the person doesn't back off,
* I would report to my superior authority about the situation.
* If my superior authority is involved in the situation I would report to the next higher authority in the chain.
