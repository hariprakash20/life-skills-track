# Focus Management

## Question 1 - What is Deep Work?
* Deep work is the ability to stay concentrated for a longer period without distraction which lets us learn hard things and create quality work quickly. 
* To be truly productive we should stay away from all social media when we do our deep work. 
* Deep work is a state of distraction-free concentration when your brain works at its maximum potential.

## Question 2 - Paraphrase all the ideas in the above videos and this one in detail.

* The optimal duration for deep work is 1 hour to 1 and a half an hour. 
* 90 minutes would be reasonable because during these 90 minutes sometime we change to something else to find or looking something in our deep work.
* Our minds argue every few minutes about whether to take a break now or later. Deadline gives us the power to overcome our minds to do the work before the deadline.
* Using exercise, cleaning, or other repetitive duties in our routines will help in learning our concepts well.
* Maintain a stimulating scoreboard to track results.
* Deep work as a professional activity is performed in a distraction-free environment that builds our cognitive abilities to their limit. 
* These efforts create and improve skills that are hard to copy.
* By practicing deep work we can upgrade our brains and allow specific brain circuits to fire effectively. This allows us to make ideas faster and find creative solutions to our problems.

## Question 3 - How can you implement the principles in your day-to-day life?

* Create a routine that involves deep work sessions.
* Working on things that are urgent and important.
* Taking breaks will help to feel refreshed and not be frustrated at the end of the day.
* Deadlines will help to reach our goals faster.
* Meditation will help our minds to relax.
* Try to develop a new habit for 30 Days
of keeping any distracting stuff away.
* Having proper sleep will help the mind work actively.
## Question 4 - Your key takeaways from the video

* Social media sites are simply a source of entertainment. Each app is designed to make our stay in that app for hours. 
* The content in those apps never gets exhausted. There are many forms of entertainment out there. 
* In case of excessive usage of social media, our attention gets fragmented frequently, which leads to a reduction in our attention span, which in turn will affect our deep work.
* The more we use social media as a distraction from depression and stress we become more lonely, isolated, inadequate, and depressed.
* Sessions of focused work can not only get more things done with higher quality but also give you far more free time to do the things you are passionate about.
