# Energy Management
## Question 1 - What are the activities you do that make you relax - Calm quadrant?
* At work time I go for a coffee or have small talk with my friends at work.
* At my home, I used to watch a movie or an episode of my favorite TV show.
## Question 2 - When do you find getting into the Stress quadrant?
* When I have not figured out a solution for the upcoming deadline.
* When I hear everyone talking about a topic that I am not familiar with.
* When I fall behind schedule.
## Question 3 - How do you understand if you are in the Excitement quadrant?
* When I complete the given assignment before the deadline.
* When I see the message that my salary is credited.
* When I complete my review or check in successfully.
## Question 4 - Paraphrase the Sleep is your Superpower video in detail.
* We need sleep before and after learning. It helps us to make remember things 40% higher and helps us make new long-term memories.
* Insufficient sleep causes us to age faster.
* Sleep deprivation causes cardiovascular diseases, reduces immune activity, and affects the reproductive system.
## Question 5 - What are some ideas that you can implement to sleep better?
* Follow a regular sleep cycle (i.e) going to bed and waking up every day at the same time.
* Avoiding blue light emitting devices before bed.
* When can fall asleep for a long time we can do some other work for a few minutes and then go to bed.
## Question 6 - Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* It improves long-term memories.
* It improves our mood.
* It keeps us healthy for the long term.
* It increases our reaction time for an activity.
* It helps us to increase our attention span.
## Question 7 - What are some steps you can take to exercise more?
* I can have a goal of exercising at least 30 minutes a day.
* I can walk an extra mile to break a sweat.
* I can join a gym and be regular.