# OSI Model

## OSI stands for Open System Interconnection.

OSI model is defined and used to understand how data is transfered from one computer to another through network.

> Platform = Operating system + microprocesser.

In case the computers are in the same platform then it can be connected via LAN. If not we have to go for OSI model.

# Layers in OSI

* Application
* Presentation
* Session
* Transport
* Network
* Data Link
* Physical
  

  
 Each layer is a package of protocals.  Lets see each layer in detail


  ## Application Layer

  It is used by network Applications(Applications that uses internet) like chrome, skype, etc..

  Application layer includes protocals like
  
  * FTP(file transfer).
  * HTTP/s(Web Surfing).
  * SMTP(Emails).
  * Telnet(Virtual Terminals)
  
  ## Presentation Layer

  Presentation layer receives data from Application layer which is in ASCCI. 

  The presentation layer performs three functions namely
  * Translation - The ASCCI code is converted into binary format.
  * Data Compression - It also reduces the size of the binary code.
  * Encryption/Decryption - SSL(Secure Sockets Layer) protocal is used in the encryption and decryption.

  ## Session Layer

  Session Layer helps in setting up and managing connections using APIs like NETBIOS(Network Basic Input Output System). It establishes connection between user and server. The functions of Session Layer are

  * Session management - manages the data download during the session.
  * Authentication - process of Identification of the user.
  * Authorization - process of determinating the permission to perform a task.

  ### Note
  The Browser handles the Application, Presentation and Session Layer.

  ## Transport Layer

  It control the reliability of the communication through

  * ### Segmentation 
    The data recevied from session layer is divided into small data units called data packets. Each packet contains Sequence number and port number. Port number helps the data to reach the correct application. Sequence number helps to arrange the data.
  
  * ### Flow Control
    It controls the amount of data being transmitted so that there is no data loss and the efficiency is high.

  * ### Error Control 
    If there is any data loss or data courruption in the receiving end an Automatic Repeat Request is sent to retrive the lost data. A group of bits called checksum is added to the packets to identify the corrupted data.

    Connection-oriented Transmission is done via Transmission Control Protocol(TCP).
     * Provides feedback.
     * relatively slow.

    Connectionless Transmission is done via User Datagram Protocol(UDP).
     * Does not provide feedback.
     * relatively fast.
  
  ## Network Layer
  Transport layer sends the data segments through the Network layer.
  The functions of Network layer are
   * ### Logical Addresssing
     Ip Addressing done in network layer is called logical addressing. Every computer in a network has a unique IP address.The network layer assigns sender's and receiver's IP address to each data segment so that we can ensure that the data has reached it's correct destination.

   * ### Routing
     Routing is the process of selecting path along which the data can be transferred from source to the destination.

   * ### path determination
     Two computers in a network can be connected in many number of ways. Finding the best path is path determination. For this protocals used are
       * OSPF(Open Shortest path First)
       * BGP(Border Gate way Protocol)
       * IS-IS(Intermediate System to Intermediate System)
  ## Data Link Layer
  Data link layer recieves data from Network Layer. Each data packet containing IP addresses of the sender and receiver.

  There are two kinds of addressing 
  * Local Addressing - is done in Network layer.
  * Physical Addressing - is done in Data Link layer where mac addresses of sender and receiver is added to each data packet to form a frame. Mac address is a 12 digit alpha numeric number embbedded insidie network interface card of each computer.
  
  Data unit in data link layer is called a frame.

  #### Note: Media is the path for the data which includes Cables, Routers and Satilites.

  ### Purpose of Data Link Layer
  * To provide access to the media shared(framing).
  * Controls how data is placed and received from the media through
    * media Access Control - Ensures Data collision does'nt happen.
    * Error Detection - Tail part of each data unit is used for error detection.
      
   
## Physical Layer.
 Data from the Data link layer is recieved by Physical layer Where the Binary bits are converted into
  * Electrical Signal - if transmitted through copper wire.
  * Light Signal - if transmitted through optical cables.
  * Radio Signal - if transmitted through Air.

Then the process happen in reverse order in the receiver side to use the transmitted data.

# References
* https://www.geeksforgeeks.org/layers-of-osi-model/
* https://www.javatpoint.com/osi-model
* https://www.youtube.com/watch?v=vv4y_uOneC0