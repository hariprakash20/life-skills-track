# Listening and Assertive Communication

## Question 1 - What are the steps/strategies to do Active Listening? 

* Avoid getting distracted by your own thoughts and foucs on the topic.
* Don't Interupt the speaking person. Let them finish first and then start talking.
* To show that your interested, Use these phrases
    * Tell me more.
    * Go ahead, I'm listening.
    * That sounds interesting!
* Show your interest through boby language.
* Take notes during important conversations.
* Summarize the topic to check wheather you are on the same page.

## Question 2 - According to Fisher's model, what are the key points of Reflective Listening? 

* Talk less listen more.
* Restate or clarify the issue stated.
* Tell what you feel or want.
* Try to understand the feeling of the content not just the fact or idea.
* have emapathy towards the speaker.
  
## Question 3 - What are the obstacles in your listening process?

* lack of empathy.
* lack of patience.
* urge to give an advice.
* lack of concentration.
  
## Question 4 - What can you do to improve your listening?

* Maintain eye contact and repeat the words with empathy.
* Ask questions.
* give feedback.
* rephrase and clarify the content disussed.
* show interest through body language.
   
## Question 5 - When do you switch to Passive communication style in your day to day life?

When I plan to go out for a movie with my friends, I would leave the decision of which movie to watch to my friends. I would be open to go any of their selection. Even if i have already watched the movie or find it boring. I just want to enjoy the time being with them. But some times I feel bad about it.

## Question 6 - When do you switch into Aggressive communication styles in your day to day life?

I switch into aggressive communication when any of my friends is not available for me when I'm in need and I'm always available for them in there time of need.

## Question 7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I switch into passive aggressive communication most of the time when someone does things that I don't like knowing that i don't like it and acting like nothing ever happened.

## Question 8 - How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

* In the situation of movie with my friends i could state my preference of movies.
* In the situation of non-availablity of my friend i could state my feelings and make them understand my situation.
* In the situation of unlikely activity by someone I could make the understand why I don't like that particular activity.

