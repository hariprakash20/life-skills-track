# Learning Process

## Question 1 - What is the Feynman Technique? Paraphrase the video in your own words.

  >Don't fool your self, you are the easiest person to fool.
  
  so the best way to understand a concept is to teach the concept to someone in layman's terms.

## Question 2 - What are the different ways to implement this technique in your learning process?

The method shown in the video seems to work for me. i.e
*  Take a paper and write the concept that you are trying to learn.
*  Write the explanation in simple words as possible.
*  Re-learn things that you feel you are not confident to explain.
*  Try to pin ponit the complicated words and try to make it simple as possible.
  
## Question 3 - Paraphrase the video in detail in your own words.

* According to the speaker there are two different modes for our brain activity.
    * ### The active state 
      where we can actively concentrate and learn and also our mind may jump between thoughts.
    * ### The difused state
      where we our mind is tired and we can dwell on a single thought for a long time.
* A slow learning process can bring in-depth knowlegde of the source material.
* Trying to recall after seeing a page of notes can help you make new neural connections and have a better memory over it.
  
## Question 4 - What are some of the steps that you can take to improve your learning process?

  * The pomodoro Technique will help us to achive these two states easily and use it effectively.
  *  Instead of being afraid to work on huge task and procrastinating, we can start working on it by starting with 25 minutes session with full focus, Not aiming to complete the task will help you make progress.

## Question 5 - Your key takeaways from the video? Paraphrase your understanding.

* The gist of the video is about learning a new skills in 20 hours and becoming an expert in it.
* Every skill has a learning curve which doesn't look good. After a certain amonut of time invested practicing a skill we reach a place where we can see any improvement and it takes a lot of time to see improvement after it.
* There are a rules the speaker suggests for learning a skill in 20 hours.
* He shares his expirence of learning a new instrument called ukulele.


## Question 6 - What are some of the steps that you can while approaching a new topic?
* The four rule the speaker suggests are
     * Deconstruct the skill. - To divide the skill into smaller parts and plan it accordingly that we can learn the skill quickly.
     * learn enough to self-correct - To have reference material and see where we go wrong.
     * remove practice barriers - avoid distractions like tv or social media.
     * Practice at least 20 hours. - dedicate a period of time to pratice a skill and being regular.


I think i can apply these steps in my life to learn a new skill.