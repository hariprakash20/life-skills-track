# Grit and Growth mindset

## Question 1 - Paraphrase (summarize) the video in a few lines. Use your own words.
* The main thing to be successful in life is grit. 
* More than talent and short time hard work, grit will make one sucessful.
* The grit is the feeling or burning desire to acheive the desired goal.
* Grit is not a sprint, it is a marathon. It is not something that we have for a week or month, It is for years.

## Question 2 - What are your key takeaways from the video to take action on?
* To have grit towards my goal.
* Keeping the goal high Enough that it takes years of hard work to acheive it.
* Hard work beats talent when talent doesn't work, So I hard work can help me acheive my goal.
  
## Question 3 - Paraphrase (summarize) the video in a few lines in your own words.
* This video is about what is growth mindset, and how can we benefit form it.
* The growth mind set is all about any skill can be working on it and it is not just talent.
* In short, skills are not born, they are built.
* Key ingredients to growth are
    * efforts
    * challenges
    * mistakes
    * feedback 
* This things are influence our focus and belief which helps us stay in the growth mindset.

## Question 4 - What are your key takeaways from the video to take action on?
* Being fixed minded and not working on my dreams would get me anywhere. So having a growth mindset and start working even if don't have any talent I could get it on the way to my goal.
* Key ingredients will help me improve my current state.
* To take care of my beliefs and focus very carefully, that's what is going to determine where I am going to be in the future.

## Question 5 - What is the Internal Locus of Control? What is the key point in the video?
* The key point in this video is to build a belief that I am in control of the my destiny (i.e) Internal locus of control, No external factor can affect my progress. So that we would always have motivation to do our work.

## Question 6 - Paraphrase (summarize) the video in a few lines in your own words.
* Beliving in ability to works things out. 
* Question your assemptions 
* Develop a life curriculam. 
* Honour the stuggle.

## Question 7 - What are your key takeaways from the video to take action on?
  Every point is a takeaway so
  * Not to be afraid of a take that is complicated or hard to understand as a thing to avoid but to belive in myself that I will be able to solve the problem if I put my time and effort into it.
  * It is important to question the negative thoughts that comes to mind like am I doing the right thing or will I be able to do that so that I can get better understanding of myself.
  * Having a goal and working towards is an important thing to do. So we have a solid reason why we are doing what we are doing.
  * It is also important to hounur our stuggles that is what brings us one step closer to our goal everyday.

## Question 8 - What are one or more points that you want to take action on from the manual? (Maximum 3)
* I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
* I will be focused on mastery till I can do things half asleep. If someone wakes me up at 3 AM in the night, I will calmly solve the problem like James Bond and go to sleep. 
* I will understand the users very well. I will serve them and the society by writing rock solid excellent software.